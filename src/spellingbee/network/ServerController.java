package spellingbee.network;

import spellingbee.server.*;

/**
 * This class will run on the server side and is used to connect the server code to the backend business code.
 * This class is where the "protocol" will be defined.
 * 
 */
public class ServerController {
	// This is the interface you will be creating!
	// Since we are storing the object as an interface, you can use any type of ISpellingBeeGame object.
	// This means you can work with either the SpellingBeeGame OR SimpleSpellingBeeGame objects and can
	// seamlessly change between the two.
	private ISpellingBeeGame spellingBee = new SpellingBeeGame();
	
	/**
	 * Action is the method where the protocol translation takes place.
	 * This method is called every single time that the client sends a request to the server.
	 * It takes as input a String which is the request coming from the client. 
	 * It then does some actions on the server (using the ISpellingBeeGame object)
	 * and returns a String representing the message to send to the client
	 * @param inputLine The String from the client
	 * @return The String to return to the client
	 * 
	 * @author	Vashti Lanz Rubio
	 * @version	12-03-20
	 * 
	 */
	public String action(String inputLine) {
		//Index of the word if inputLine is split.
		final int WORD_INDEX = 1;
		
		if(inputLine.startsWith("getAllLetters")) {
			return spellingBee.getAllLetters();
		}
		if(inputLine.startsWith("wordCheck")) {
			String[] inputLine_split = inputLine.split(":");
			return spellingBee.getMessage(inputLine_split[WORD_INDEX]) + ":" + spellingBee.getPointsForWord(inputLine_split[WORD_INDEX]);
		}
		if(inputLine.startsWith("getCenter")) {
			return  ""+ spellingBee.getCenterLetter();
		}
		if(inputLine.startsWith("getBrackets")) {
			int[] brackets = spellingBee.getBrackets();
			return brackets[0] + ":" + brackets[1] + ":" + brackets[2] + ":" + brackets[3] + ":" + brackets[4];
		}
		if(inputLine.startsWith("GetScore")) {
			return "" + spellingBee.getScore();
		}
		return null;
	}
}
