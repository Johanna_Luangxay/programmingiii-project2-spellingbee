package spellingbee.server;

/**
 * Class that implements on ISpellingBeeGame. It is a simpler version of the
 * SpellingBeeGame where the letters are hard-coded, as well as the possible
 * words.
 * 
 * @author Johanna Luangxay
 * @version 2020-12-01
 *
 */
public class SimpleSpellingBeeGame implements ISpellingBeeGame{

	private String allLetters;
	private char centerLetter;
	private int score;
	private String[] possibleWords = new String[4];
	
	/**
	 * The constructor
	 */
	public SimpleSpellingBeeGame()
	{
		allLetters = "TBILMOY";
		centerLetter = 'T';
		
		//Fill the array of possible word to test
		possibleWords[0] = "BOOT";
		possibleWords[1] = "IMMOBILITY";
		possibleWords[2] = "BOTTOM";
		possibleWords[3] = "LOBOTOMY";
	}
	
	/**
	 * Gives the number of points earned depending on the attempt according to the SpellingBeeGame rules
	 * @param attempt get the attempt of the user
	 * @return int returns the number of point earned for the attempts
	 */
	@Override
	public int getPointsForWord(String attempt) 
	{
		//Makes everything upper case and case insensitive
		attempt = attempt.toUpperCase();
		int point = 0;
		final int MINIMUM_LENGTH = 4;
		int wordLength = attempt.length();
		String goodResponse = "Valid Entry";
		
		//Use function verifyAttempt to know if the word is valide
		if(verifyAttempt(attempt).equals(goodResponse))
		{	
			final int BONUS_POINT = 7;
			//Check with function containLetters if attempt uses all letters
			if(containLetters(attempt))
			{
				point = wordLength+BONUS_POINT;
			}
			else
			{
				//If wordLength = 4, 1 point only
				if(wordLength == MINIMUM_LENGTH)
				{
					final int MINIMUM_POINT = 1;
					point = MINIMUM_POINT;
				}
				else
				{
					point = wordLength;
				}
			}					
		}
		//Update the score
		score = score+point;
		return point;
	}
	
	/**
	 * Check if the attempt uses all letters
	 * @param attempt word to verify
	 * @return boolean Tells if the attempt used all letters
	 */
	private boolean containLetters(String attempt)
	{
		String[] letterArray = allLetters.split("");
		//Loop through the letterArray and look if the letter is among the attempt word
		for(String letter : letterArray)
		{
			if(!attempt.toUpperCase().contains(letter))
			{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Look if the word is valid or not, and returns a message
	 * @param attempt Word to verify
	 * @return String Return message that explains if it's valid or not
	 */
	private String verifyAttempt(String attempt)
	{
		String response;
		attempt = attempt.toUpperCase();
		
		//If there is no center letters
		if (!(attempt.contains(String.valueOf(centerLetter))))
		{
			response = "Invalid Entry - Does not contain the center letter.";;
			return response;
		}
		
		//If the length is smaller than 4
		if(attempt.length()<4)
		{
			response = "Invalid Entry - The word is too short.";
			return response;
		}
		
		//If a letter is not part of the given letter
		String[] attemptLetters = attempt.split("");
		for (String letter : attemptLetters)
		{
			if(!(allLetters.contains(letter)))
			{
				System.out.println(!(allLetters.contains(letter)));
				System.out.println(letter);
				response = "Invalid Entry - At least one letter is not a given letter.";
				return response;
			}
		}
		
		
		//If the word is part of the possible words and it passed all the other condition, it's a valid entry
		for (String possibleWord : possibleWords)
		{
			if(attempt.equals(possibleWord.toUpperCase()))
			{
				response = "Valid Entry";
				return response;
			}
		}
		
		//If it wasn't a valid entry, only condition left is that it's not part of the possible words
		response = "Invalid Entry - Not considered as a word.";
		return response;
	}
	
	/**
	 * Get the message from the verifyAttempt and gives it back
	 * @param attempt Word to get message from
	 * @return String message to be shown
	 */
	@Override
	public String getMessage(String attempt)
	{
		attempt = attempt.toUpperCase();
		String message = verifyAttempt(attempt);
		return message;
		
	}
	
	/**
	 * Gives back all the letters
	 * @return String All the letters
	 */
	@Override
	public String getAllLetters()
	{
		return allLetters;
	}
	
	/**
	 * Gives the center letter
	 * @return char Center Letter
	 */
	@Override
	public char getCenterLetter()
	{
		return centerLetter;
	}
	
	/**
	 * Returns the score (total of points)
	 * @return int Score
	 */
	@Override
	public int getScore()
	{
		return score;
	}
	
	/**
	 * Returns how many points to get each title
	 * @return int[] Brackets
	 */
	@Override
	public int[] getBrackets()
	{
		int[] brackets = new int[] {1,6,14,31,32};
		return brackets;
	}
	
}
