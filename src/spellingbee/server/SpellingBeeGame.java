package spellingbee.server;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.io.IOException;
import java.nio.*;
import java.nio.file.*;
import java.util.Random;
import java.util.Set;

/**
 * This class implements the ISpellingBeeGame interface. It contains methods
 * necessary for spelling bee and validating words based on spelling bee rules.
 * 
 * @author	Vashti Lanz Rubio
 * @version	12-03-20
 * */
public class SpellingBeeGame implements ISpellingBeeGame{
	private String[] allLetters;
	private char centerLetter;
	private int score;
	private Collection<String> wordsFound;
	private static Collection<String> possibleWords = createWordsFromFile("datafiles\\english.txt");
	private int maxScore;
	private boolean isInitialized;
	
	/**
	 * This function reads the text file, which represents the dictionary, from the path
	 * and returns a List containing words within that text file.
	 * 
	 * @param	path	The path of the text file
	 * */
	private static Set<String> createWordsFromFile(String path){
		try {
		List<String> wordsList = new ArrayList<String>();
		
		Path p = Paths.get(path);
		wordsList = Files.readAllLines(p);
		
		//Set<String> wordsSet = new HashSet<String>(wordsList);
		Set<String> wordsSet = new HashSet<String>();
		for(String s : wordsList) {
			wordsSet.add(s.toUpperCase());
		}
		return wordsSet;
		} catch (IOException e) {
			System.out.println("english.txt not found");
			System.exit(0);
		}
		return null;
	}
	/**
	 * This constructor takes no input. It initializes necessary 
	 * variables to be used inside this class.
	 * */
	public SpellingBeeGame() {
		try {
			List<String> list = new ArrayList<String>();
			Random ran = new Random();
			Path p = Paths.get("datafiles\\letterCombinations.txt");
			list = Files.readAllLines(p);

			//Randomly gets a String in the list from 0 to size of list minus 1
			//Then splits that into a String array containing the seven letters.
			String combination = list.get(ran.nextInt(list.size())).toUpperCase();
			allLetters = combination.split("");
		
			//Randomly gets a character in combination and will be used as the centerLetter.
			centerLetter = combination.charAt(ran.nextInt(combination.length()));
		
			isInitialized = false;
			score = 0;
			wordsFound = new HashSet<String>();
			computeMaxScore();
		} catch(IOException e) {
			System.out.println("letterCombiantions.txt not found");
			System.exit(0);
		}
	}
	/**
	 * This constructor takes an input of String. This input represents 
	 * the seven letters to be used in spelling bee. The constructor initializes 
	 * necessary variables to be used inside this class.
	 * 
	 * @param	allLetters	String of seven letters.
	 * */
	public SpellingBeeGame(String allLetters) {
		Random ran = new Random();

		this.allLetters = allLetters.toUpperCase().split("");
		//Randomly gets a character in combination and will be used as the centerLetter.
		centerLetter = allLetters.toUpperCase().charAt(ran.nextInt(allLetters.length()));
			
		isInitialized = false;
		score = 0;
		wordsFound = new HashSet<String>();
		computeMaxScore();
	}
	/**
	 * This method returns the number of points that a given word 
	 * is worth according to the Spelling Bee rules.
	 * 
	 * @param	attempt	String of the word that wants to be evaluated
	 * @return	int	number of points that the given word is worth
	 */
	public int getPointsForWord(String attempt) {
		final String VALID_MESSAGE = "Valid Entry";
		int points = 0;
		if(!getMessage(attempt).equals(VALID_MESSAGE)) {
			return points;
		}
		final int BONUS_POINTS = 7;
		final int WORD_LENGTH_MINUMUM = 4;
		final int MINIMUM_POINT = 1;
		
		if(attempt.length() > WORD_LENGTH_MINUMUM) {
			points = attempt.length();
			//Bonus points if word(attempt) contains all seven letters
			if(containsAllLetters(attempt)) {
				points += BONUS_POINTS;
			}
		}
		if(attempt.length() == WORD_LENGTH_MINUMUM) {
			points = MINIMUM_POINT;
		}
		if(isInitialized) {
			wordsFound.add(attempt.toUpperCase());
			score += points;
		}
		return points;
	}
	/**
	 * This function returns the set of 7 letters (as a String) that 
	 * the spelling bee object is storing.
	 * 
	 * @return 	String 	Set of 7 letters as a String.
	 */
	public String getAllLetters(){
		String letters = "";
		for(String s : allLetters) {
			letters += s;
		}
		return letters;
	}
	/**
	 * This function is used in the GUI to determine 
	 * the various bracket categories.
	 * @return	int[]	Various Point Categories
	 */
	public int[] getBrackets() {
		final int CATEGORY_COLUMN = 5;
		final double TWENTY_FIVE_PERCENT = .25;
		final double FIFTY_PERCENT = .50;
		final double SEVENTY_FIVE_PERCENT = .75;
		final double NINETY_PERCENT = .90;
		
		int[] arr = new int[CATEGORY_COLUMN];
		arr[0] = (int) (maxScore * TWENTY_FIVE_PERCENT);
		arr[1] = (int) (maxScore * FIFTY_PERCENT);
		arr[2] = (int) (maxScore * SEVENTY_FIVE_PERCENT);
		arr[3] = (int) (maxScore * NINETY_PERCENT);
		arr[4] = maxScore;
				
		return arr;
	}
	/**
	 * This functions returns the center letter as a char that is 
	 * required to be part of every word.
	 * 
	 * @return	char	The center letter.
	 */
	public char getCenterLetter() {
		return centerLetter;
	}
	/**
	 * This function returns the current score of the user.
	 * 
	 * @return	int	Score of the user.
	 */
	public int getScore() {
		return score;
	}
	/**
	 * This method checks if the word attempt is a valid word or not according to the Spelling Bee rules. 
	 * It should return a message based on the reason it is rejected or a positive message .
	 * 
	 * @param	attempt	String of the word that wants to be evaluated
	 * @return	String	Message on either the reason of invalidity or declaration of valid word
	 */
	public String getMessage(String attempt) {
		/*Returns bad if:
		 *  attempt word does not contain the center letter
		 *  attempt word is already found.
		 *  attempt word is not in the list of possible words.
		 *  length of word is less than 4
		*/
		final int WORD_LENGTH_MINUMUM = 4;
		if(attempt.length() < WORD_LENGTH_MINUMUM) {
			return "Invalid Entry - The word is too short.";
		}

		if(!attempt.toUpperCase().contains("" + centerLetter)) {
			return "Invalid Entry - Does not contain the center letter.";
		}
		if(wordsFound.contains(attempt.toUpperCase())) {
			return "Invalid Entry - Word already found.";
		}
		if(!possibleWords.contains(attempt.toUpperCase())) {
			return "Invalid Entry - Not considered as a word.";
		}
		//Returns bad if attempt word uses letters not in allLetters
		String[] attempt_letters = attempt.toLowerCase().split("");
		for(String letter : attempt_letters) {
			boolean isBad = true;
			//For each letter, verify if it is in the list of usable letters
			for(String valid_letter : allLetters) {
				if(letter.equalsIgnoreCase(valid_letter)) {
					isBad = false;
					break;
				}
			}
			if(isBad) {
				return "Invalid Entry - At least one letter is not a given letter.";
			}
		}
		
		return "Valid Entry";
	}
	/**
	 * This method calculates and returns the max score 
	 * that is obtainable using the letters of allLetters.
	 * 
	 * @return	int	The max score obtainable.
	 * */
	private void computeMaxScore() {
		for(String word : possibleWords) {
			maxScore += getPointsForWord(word);
		}
		isInitialized = true;
	}
	/**
	 * This method checks if the word contains the seven letters required.
	 * 
	 * @param	word	The word
	 * @return	boolean	True or false depending of the word contains all the letters.
	 */
	private boolean containsAllLetters(String word) {
		for(String letter : allLetters) {
			if(!word.toUpperCase().contains(letter)) {
				return false;
			}
		}
		return true;
	}
}
