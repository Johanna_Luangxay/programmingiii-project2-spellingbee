package spellingbee.server;

/**
 * Interface ISpellingBeeGame have all the methods need to storing a spelling bee game.
 * @author Johanna Luangxay
 * @version 2020-11-26
 *
 */
public interface ISpellingBeeGame {
	/**
	 * Should return the number of points that a given word is worth according to the Spelling Bee rules.
	 * @param attempt String of the word that wants to be evaluated
	 * @return int number of points that the given word is worth
	 */
	int getPointsForWord(String attempt);
	
	/**
	 * Should check if the word attempt is a valid word or not according to the Spelling Bee rules. 
	 * It should return a message based on the reason it is rejected or a positive message 
	 * (e.g. �good� or �great�) if it is a valid word.
	 * @param attempt String of the word that wants to be evaluated
	 * @return String Message on either the reason of invalidity or declaration of valid word
	 */
	String getMessage(String attempt);
	
	/**
	 * This method should return the set of 7 letters (as a String) that the spelling bee object is storing
	 * @return String set of 7 letters that the spelling bee object is storing
	 */
	String getAllLetters();
	
	/**
	 * Should return the center character. 
	 * That is, the character that is required to be part of every word.
	 * @return char Character that is required to be part of every word
	 */
	char getCenterLetter();
	
	/**
	 * This method should return the current score of the user.
	 * @return int Score of the user
	 */
	int getScore();
	
	/**
	 * This method will be used in the GUI to determine the various point categories.
	 * @return int[] Various Point Categories
	 */
	int[] getBrackets();
}
