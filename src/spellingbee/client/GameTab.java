package spellingbee.client;

import javafx.event.ActionEvent;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import spellingbee.network.Client;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;

/**
 * This class creates the game tab. The user will interact with this tab mostly.
 * It extends the Tab class
 * 
 * @author Johanna Luangxay
 * @version 2020-12-02
 *
 */
public class GameTab extends Tab{
	
	private Client user;
	final private GridPane gp = new GridPane();;
	
	private String centerLetter;
	private String[] normalLetters; 
	private int score;
	final private int TOTAL_LETTERS=7;
	
	final private Button[] lettersBtn = new Button[TOTAL_LETTERS];
	final private Button submitBtn = new Button("Submit");
	final private TextField userInputTxt = new TextField();
	final private TextField messageTxt = new TextField("Message: ");
	final private TextField scoreTxt = new TextField("Score: ");	
	
	/**
	 * The Constructor
	 * @param user A client object
	 */
	public GameTab(Client user)
	{
		super("Game");
		this.user = user;
		
		centerLetter = user.sendAndWaitMessage("getCenterLetter").toUpperCase();
		
		//Remove center letter and set the normalLetters array without it.
		removeCenterLetter();
		
		//Add the buttons and TextFields to the game tab
		addElements();
			
		setContent(gp);
	}
	
	/**
	 * Remove the center letter and set the normalLetters array without it.
	 */
	private void removeCenterLetter()
	{
		String response = user.sendAndWaitMessage("getAllLetters").toUpperCase();
		
		//Get the allLetters array
		String[] allLetters = response.split(""); 
		
		//Get the index of the center letter in the array
		int indexCenter = response.indexOf(centerLetter);

		final int NUMBER_NORMAL_LETTER = 6;
		
		//Create new array with normal letters only
		normalLetters = new String[NUMBER_NORMAL_LETTER];
		
		//Adds the value to the new array without the center letter
		int position = 0;
		for(int i = 0; i<TOTAL_LETTERS;i++)
		{
			if(i != indexCenter)
			{
				normalLetters[position] = allLetters[i];
				position++;
			}
		}

	}
	
	/**
	 * Adds the elements to the game tab
	 */
	private void addElements()
	{
		//Make the TextFields the same length as the scene
		messageTxt.setPrefWidth(500);
		scoreTxt.setPrefWidth(500);
		userInputTxt.setPrefWidth(500);
		
		//Add the buttons
		gp.add(userInputTxt, 0,1,7,1);
		gp.add(submitBtn, 0,2,3,1);
		
		//Number of empty rows between input and output
		final int NUMBER_EMPTY_ROWS = 5;
		final int CURRENT_ROW = 3;
		//Adds the empty rows
		for(int i=0;i<NUMBER_EMPTY_ROWS;i++)
		{
			gp.addRow(CURRENT_ROW+i,new Text(""));
		}
		
		//Add the TextFields to the tab
		gp.add(messageTxt, 0, 8,7,1);				
		gp.add(scoreTxt, 0, 9,7,1);
		
		addButtons();
		addSubmitEvent();
		
	}
	/**
	 * Add the buttons to the scene
	 */
	private void addButtons()
	{
		final int INDEX_CENTER= 0;
		for(int i=0;i<TOTAL_LETTERS;i++)
		{
			String currentLetter;
			//Determines current letter to be add
			if(i==INDEX_CENTER)
			{
				currentLetter = centerLetter;
			}
			else
			{
				//i-1 because in the normalLetters array, there is 6 letters and not 7
				currentLetter = normalLetters[i-1];
			}
			//Add the button
			lettersBtn[i] = new Button(currentLetter);
			gp.add(lettersBtn[i], i,0,1,1);
			
			//Set the action of the button
			lettersBtn[i].setOnAction(new EventHandler<ActionEvent>()
			{
				@Override
				public void handle(ActionEvent e) 
				{
					userInputTxt.setText(userInputTxt.getText()+currentLetter);
				}
			}
			);
			//Add the styling, color to the center letter button
			if(i==INDEX_CENTER)
			{
				lettersBtn[i].setStyle("-fx-text-fill: #FF0000");
			}
		}
	}
	
	/**
	 * Add the event to the submit button
	 */
	private void addSubmitEvent()
	{
		submitBtn.setOnAction(new EventHandler<ActionEvent>() 
		{
	         @Override
	         public void handle(ActionEvent e) 
	         {
	        	 final int MESSAGE_INDEX= 0;
	        	 final int SCORE_INDEX= 1;
	        	 //Gets the string response
	        	 String response = user.sendAndWaitMessage("wordCheck:"+userInputTxt.getText());
	        	 //Adds the message to the TextField
	        	 String messageResponse = response.split(":")[MESSAGE_INDEX];
	        	 messageTxt.setText("Message: "+messageResponse);
	        	 //Adds the points to the score TextField
	        	 int pointResponse = Integer.parseInt(response.split(":")[SCORE_INDEX]);
	        	 score = score+pointResponse;
	        	 scoreTxt.setText("Score: "+score);
	         }          
		}
	);
	}
	
	/**
	 * Returns the scoreTxt TextField
	 * @return TextField for the ScoreTab to have access to it
	 */
	public TextField getScoreTxt()
	{
		return scoreTxt;
	}
	
}
