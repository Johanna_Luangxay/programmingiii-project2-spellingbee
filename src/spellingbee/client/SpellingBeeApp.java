package spellingbee.client;

import spellingbee.network.Client;
/**
 * This class contains the Client object
 * necessary for the Tab Classes.
 * 
 * @author	Vashti Lanz Rubio
 * @version	11-26-20
 * */
public class SpellingBeeApp {
	private Client c;
	/**
	 * This constructor initializes the Client object
	 */
	public SpellingBeeApp() {
		c = new Client();
	}
	/**
	 * This method returns the Client object
	 */
	public Client getClient() {
		return c;
	}
}
