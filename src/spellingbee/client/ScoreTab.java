package spellingbee.client;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import spellingbee.network.Client;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;
/**
 * This class extends to Tab. It is used to create
 * a Tab object that represents the score of spelling bee.
 * 
 * @author	Vashti Lanz Rubio
 * @version	12-03-20
 */
public class ScoreTab extends Tab{
	private Client client;
	private GridPane gp;
	
	private Text queen_bee_label;
	private Text genius_label;
	private Text amazing_label;
	private Text good_label;
	private Text getting_started_label;
	private Text currentScore_label;
	
	private Text queen_bee_score;
	private Text genius_score;
	private Text amazing_score;
	private Text good_score;
	private Text getting_started_score;
	private Text currentScore_score;
	
	private String[] brackets;
	/**
	 * The constructor.
	 * 
	 * @param	client	A Client object
	 */
	public ScoreTab(Client client) {
		super("Score");
		this.client = client;
		brackets = getBrackets();
		createTextLabels();
		createTextScores();
		createGridPane();
		this.setContent(gp);
	}
	/**
	 * This method initializes and creates a GridPane field
	 * as well as adding the Text objects within it
	 */
	private void createGridPane() {
		gp = new GridPane();
		gp.setHgap(10);
		gp.setVgap(2);
		
		addTextLabels();
		addTextScores();
	}
	/**
	 * This method adds Text objects that are used
	 * as a label in the GridPane.
	 */
	private void addTextLabels() {
		gp.add(queen_bee_label,0,0);
		gp.add(genius_label,0,1);
		gp.add(amazing_label,0,2);
		gp.add(good_label,0,3);
		gp.add(getting_started_label,0,4);
		gp.add(currentScore_label,0,5);
	}
	/**
	 * This method initializes and creates Text fields
	 * that are used as a label.
	 */
	private void createTextLabels() {
		queen_bee_label = new Text("Queen Bee");
		genius_label = new Text("Genius");
		amazing_label = new Text("Amazing");
		good_label = new Text("Good");
		getting_started_label = new Text("Getting started");
		currentScore_label = new Text("Current Score");
		styleTextLabels();
	}
	/**
	 * This method adds styling the Text Objects
	 * that are used as a label.
	 */
	private void styleTextLabels() {
		queen_bee_label.setFill(Color.GREY);
		genius_label.setFill(Color.GREY);
		amazing_label.setFill(Color.GREY);
		good_label.setFill(Color.GREY);
		getting_started_label.setFill(Color.GREY);
	}
	/**
	 * This method initializes and creates Text fields
	 * that are used as to display brackets and score.
	 */
	private void createTextScores() {
		final int GETTING_STARTED_BRACKET = 0;
		final int GOOD_BRACKET = 1;
		final int AMAZING_BRACKET = 2;
		final int GENIUS_BRACKET = 3;
		final int QUEEN_BEE_BRACKET = 4;
		
		queen_bee_score = new Text(brackets[QUEEN_BEE_BRACKET]);
		genius_score = new Text(brackets[GENIUS_BRACKET]);
		amazing_score = new Text(brackets[AMAZING_BRACKET]);
		good_score = new Text(brackets[GOOD_BRACKET]);
		getting_started_score = new Text(brackets[GETTING_STARTED_BRACKET]);
		currentScore_score = new Text(this.retrieveScore());
		currentScore_score.setFill(Color.RED);
	}
	/**
	 * This method adds Text objects that are used
	 * to display the brackets and score in the GridPane.
	 */
	private void addTextScores() {
		gp.add(queen_bee_score,1,0);
		gp.add(genius_score,1,1);
		gp.add(amazing_score,1,2);
		gp.add(good_score,1,3);
		gp.add(getting_started_score,1,4);
		gp.add(currentScore_score,1,5);
	}
	/**
	 * This method calls the server to receive and return the brackets
	 * as a String
	 * 
	 * @return	String[] Array of numbers that represent the brackets
	 */
	private String[] getBrackets() {
		String[] brackets = client.sendAndWaitMessage("getBrackets").split(":");
		return brackets;
	}
	/**
	 * This method updates the GUI by updating the score value
	 * as well as the brackets design if user goes above a bracket.
	 */
	public void refresh() {
		String score = this.retrieveScore();
		currentScore_score.setText(score);
		updateBrackets(Integer.parseInt(score));
	}
	/**
	 * This method updates the color of the text labels
	 * if the score was about its requirement.
	 * 
	 * @param	score	The current score of the user
	 */
	private void updateBrackets(int score) {
		final int GETTING_STARTED_BRACKET = 0;
		final int GOOD_BRACKET = 1;
		final int AMAZING_BRACKET = 2;
		final int GENIUS_BRACKET = 3;
		final int QUEEN_BEE_BRACKET = 4;
		
		if(score >= Integer.parseInt(brackets[GETTING_STARTED_BRACKET])) {
			getting_started_label.setFill(Color.BLACK);
		}
		if(score >= Integer.parseInt(brackets[GOOD_BRACKET])) {
			good_label.setFill(Color.BLACK);
		}
		if(score >= Integer.parseInt(brackets[AMAZING_BRACKET])) {
			amazing_label.setFill(Color.BLACK);
		}
		if(score >= Integer.parseInt(brackets[GENIUS_BRACKET])) {
			genius_label.setFill(Color.BLACK);
		}
		if(score >= Integer.parseInt(brackets[QUEEN_BEE_BRACKET])) {
			queen_bee_label.setFill(Color.BLACK);
		}
	}
	/**
	 * This method gets the score from the server
	 * and returns it.
	 */
	private String retrieveScore() {
		return client.sendAndWaitMessage("GetScore");
	}
}
