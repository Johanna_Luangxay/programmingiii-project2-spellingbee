package spellingbee.client;

import java.util.List;
import javafx.application.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

/**
 * This class is generates the GUI for the spelling bee game.
 * @author Vashti Lanz Rubio
 * @author Johanna Luangxay
 * @version 2020-12-03
 *
 */
public class SpellingBeeClient extends Application{
	/**
	 * Creates the stage and elements in it
	 */
	public void start(Stage stage) {
		Group root = new Group(); 
		
		//Dimensions of the scene
		double height = 500;
		double width = 500;
		
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, width, height); 
		scene.setFill(Color.WHITE);
		
		//Tabs
		SpellingBeeApp client = new SpellingBeeApp();
		TabPane tp = new TabPane();
		GameTab gameTab = new GameTab(client.getClient());
		ScoreTab scoreTab = new ScoreTab(client.getClient());
		
		gameTab.getScoreTxt().textProperty().addListener(new ChangeListener<String>() {
															@Override
	         												public void changed(ObservableValue<? extends String> observable, String old, String newV){
																scoreTab.refresh();
	         												}
														}
		);
																	
		
		tp.getTabs().add(gameTab);
		tp.getTabs().add(scoreTab);
		tp.setPrefWidth(width);
		
		root.getChildren().add(tp);

		//Associate scene to stage and show
		stage.setTitle("Spelling Bee!"); 
		stage.setScene(scene); 
		stage.show(); 
	}
	/**
	 * Launch the stage.
	 * @param args
	 */
    public static void main(String[] args) {
        Application.launch(args);
    }
}
